'''
Urllib module is the URL handling module for python.
It is used to fetch URLs (Uniform Resource Locators).
It uses the urlopen function and is able to fetch URLs using a
variety of different protocols.
'''

import urllib.request

'''
The JSON format is often used for serializing and transmitting structured
data over a network connection.
It is used primarily to transmit data between a server and web application,
serving as an alternative to XML. JSON is JavaScript Object Notation.
'''
import json

# API use "https://newsapi.org/"

#key = '7d64060443474e59a13bcba152f0a3c1'
''' get url of below newsapi.org with api key '''
url = 'https://newsapi.org/v2/everything?q=bitcoin&from=2019-12-09&sortBy=publishedAt&apiKey=b2f79da301c04dcbbe5c141d3ee2afe0'
json_obj = urllib.request.urlopen(url).read() # Open and read that url
data = json.loads(json_obj)  # load and return that data in json format


for item in data['articles']:
    #print(item)  Return all the data with all info
    print ("Author is   :",item['author'])
    print ("Title is   :",item['title'])
    print ("publishedAt :",item['publishedAt'])
    print ("Url is   :",item['url'])
    print('\n')
